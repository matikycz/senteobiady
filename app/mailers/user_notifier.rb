class UserNotifier < ApplicationMailer
	default from: "Sente obiady <senteobiady@sente.pl>"

	def favorite_restaurant_order(user, order)
		@user = user
		@order = order
		mail(to: @user.email, subject: "Zamówienie z Twojej ulubionej restauracji")
	end

	def order_status_change(user, order)
		@user = user
		@order = order
		mail(to: @user.email, subject: "Zamówienie do którego się doczepiłeś")
	end

	def private_message(user, order, message, author)
		@user = user
		@order = order
		@creator = author
		@message = message
		mail(to: @user.email, subject: "Prywatna wiadomość od #{@creator.name}")
	end
end
