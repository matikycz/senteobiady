class Restaurant < ActiveRecord::Base
	validates :name, length: { in: 4..100 }
	validates :link, length: { in: 4..300 }

	has_many :favorites
	has_many :orders
end
