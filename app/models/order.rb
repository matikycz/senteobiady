class Order < ActiveRecord::Base

  belongs_to :user
  belongs_to :restaurant
  has_many :order_positions
  enum status: {
		nowe: 1,
		zamowione: 2,
		dostarczone: 3,
    anulowane: 4
		}
end
