class OrderPosition < ActiveRecord::Base
  validates :name, length: { minimum: 4 }
  validates :cost, presence: true, numericality: { greater_than: 0 }
  belongs_to :order
  belongs_to :user
end
