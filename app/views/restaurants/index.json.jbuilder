json.array!(@restaurants) do |restaurant|
  json.extract! restaurant, :id, :name, :link, :description
  json.url restaurant_url(restaurant, format: :json)
end
