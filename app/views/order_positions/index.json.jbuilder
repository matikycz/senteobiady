json.array!(@order_positions) do |order_position|
  json.extract! order_position, :id, :order_id, :user_id, :name, :cost
  json.url order_position_url(order_position, format: :json)
end
