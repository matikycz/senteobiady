class DebtsController < ApplicationController
  def index
  	@mydebets = []
  	current_user.order_positions.where('paid is null or cost != paid').each do |e|
  		if e.order.user != current_user
  			@mydebets.push(e)
  		end
  	end

  	@otherdebets = []
  	current_user.orders.each do |e|
  		e.order_positions.each do |e1|
  			if((e1.cost!= e1.paid or !e1.paid.present?) and e1.user != current_user )
  				@otherdebets.push(e1)
  			end
  		end
  	end
  end

  def edit
  	@order_position = OrderPosition.find(params[:id])
  	if @order_position.order.user != current_user or @order_position.cost == @order_position.paid
  		redirect_to debts_path, notice: 'Nie możesz rozliczać tej pozycji'
  	end
  end

  def update
  	@order_position = OrderPosition.find(params[:id])
  	if @order_position.order.user != current_user
  		redirect_to debts_path, notice: 'Nie możesz rozliczać tej pozycji'
  	end
  	paid = params[:order_position][:paid]
  	if @order_position.cost == @order_position.paid
  		redirect_to debts_path, notice: 'Pozycja już rozliczona'
  	elsif paid.to_f > @order_position.cost||=0
  		redirect_to edit_debts_path(@order_position), notice: 'Użytkownik nie mógł zapłacić więcej niż wynosi koszt pozycji'
  	else
  		@order_position.paid = paid
  		if @order_position.save
  			redirect_to debts_path, notice: 'Pomyślnie rozliczono'
  		else
  			redirect_to debts_path, notice: 'Błąd podczas rozliczania'
  		end
  	end
  end
end
