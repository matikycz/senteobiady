class CallbacksController < Devise::OmniauthCallbacksController
  def google_oauth2
    @user = User.from_omniauth(request.env["omniauth.auth"])
    domain = @user.email.split('@')[1]
    if @user.persisted? and domain == "sente.pl"
    	flash[:notice] = "Zalogowano pomyślnie"
    	sign_in_and_redirect @user, :event => :authentication
  	else
  		flash[:warning] = "Musisz zalogowaś się z domeny sente.pl, aby przejść autoryzację."
      redirect_to new_user_session_url
    end
  end
end