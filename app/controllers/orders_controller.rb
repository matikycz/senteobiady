class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.where('ordertime > ?', DateTime.now.beginning_of_day).all
  end

  def today
    @orders = Order.where("ordertime >= #{Date.today}")
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
    if @order.user != current_user
      redirect_to @order, notice: 'Nie jesteś autorem zamówienia'
    end
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)
    @order.user = current_user
    @order.status = "nowe"
    respond_to do |format|
      if @order.save
        Favorite.where(restaurant_id: @order.restaurant.id).where.not(user_id: current_user.id).each do |favorite|
          UserNotifier.favorite_restaurant_order(favorite.user, @order).deliver_now
        end
        Setting.where.not(user: current_user).where(mailing: 1).each do |setting|
          UserNotifier.favorite_restaurant_order(setting.user, @order).deliver_now
        end
        format.html { redirect_to @order, notice: 'Zamówienie zostało założone.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    if @order.user != current_user
      redirect_to @order, notice: 'Nie jesteś autorem zamówienia'
    end
    puts '--------'
    p order_params
    puts '--------'
    
    @order.restaurant_id = order_params[:restaurant_id]
    @order.ordertime = DateTime.civil_from_format(
        :local,
        order_params["ordertime(1i)"].to_i,
        order_params["ordertime(2i)"].to_i,
        order_params["ordertime(3i)"].to_i,
        order_params["ordertime(4i)"].to_i,
        order_params["ordertime(5i)"].to_i
    )
    @order.desctiption = order_params[:desctiption]
    @order.status = order_params[:status]
    if @order.valid? and @order.status_changed?
      @order.order_positions.each do |position|
        if position.user != @order.user
          UserNotifier.order_status_change(position.user, @order).deliver_now
        end
      end
    end
    respond_to do |format|
      if @order.save
        if @order.status_changed?
          @order.order_positions.each do |position|
            UserNotifier.order_status_change(position.user, @order).deliver_now
          end
        end
        format.html { redirect_to @order, notice: 'Pomyślnie edytowałeś zamówienie' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    if @order.user != current_user
      redirect_to @order, notice: 'Nie jesteś autorem zamówienia'
    end
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:user_id, :restaurant_id, :ordertime, :desctiption, :status)
    end
end
