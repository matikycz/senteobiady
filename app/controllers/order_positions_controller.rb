class OrderPositionsController < ApplicationController
  before_action :set_order_position, only: [:show, :edit, :update, :destroy]

  # GET /order_positions
  # GET /order_positions.json
  def index
    @order_positions = Order.find(params[:order_id]).order_positions
  end

  # GET /order_positions/1
  # GET /order_positions/1.json
  def show
  end

  # GET /order_positions/new
  def new
    @order = Order.find(params[:order_id])
    if @order.status != 'nowe'
      redirect_to @order, notice: 'Możesz dodawać pozycje tylko do zamówień o statusie nowe'
    end
    @order_position = OrderPosition.new
  end

  # GET /order_positions/1/edit
  def edit
    @order = Order.find(params[:order_id])
    if @order.status != 'nowe'
      redirect_to @order, notice: 'Możesz edytować pozycje tylko do zamówień o statusie nowe'
    end
    @order_position = OrderPosition.find(params[:id])
  end

  # POST /order_positions
  # POST /order_positions.json
  def create
    @order_position = OrderPosition.new(order_position_params)
    @order = Order.find(params[:order_id])
    if @order.status != 'nowe'
      redirect_to @order, notice: 'Możesz dodawać pozycje tylko do zamówień o statusie nowe'
    end
    @order_position.order = @order
    @order_position.user = current_user
    respond_to do |format|
      if @order_position.save
        format.html { redirect_to @order, notice: 'Pomyślnie dodano pozycję do zamówienia.' }
        format.json { render :show, status: :created, location: @order_position }
      else
        format.html { render :new }
        format.json { render json: @order_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /order_positions/1
  # PATCH/PUT /order_positions/1.json
  def update
    @order = Order.find(params[:order_id])
    if @order.status != 'nowe'
      redirect_to @order, notice: 'Możesz dodawać pozycje tylko do zamówień o statusie nowe'
    end
    respond_to do |format|
      if @order_position.update(order_position_params)
        format.html { redirect_to @order, notice: 'Pozycja pomyślnie edytowana.' }
        format.json { render :show, status: :ok, location: @order_position }
      else
        format.html { render :edit }
        format.json { render json: @order_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /order_positions/1
  # DELETE /order_positions/1.json
  def destroy
    @order = Order.find(params[:order_id])
    if @order.status != 'nowe'
      redirect_to @order, notice: 'Możesz usuwać pozycje tylko z zamówień o statusie nowe'
    end
    @order_position.destroy
    respond_to do |format|
      format.html { redirect_to @order, notice: 'Order position was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_position
      @order_position = OrderPosition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_position_params
      params.require(:order_position).permit(:name, :cost)
    end
end
