class FavoritesController < ApplicationController
  before_action :set_favorite, only: [:show, :edit, :update, :destroy]

  # GET /favorites
  # GET /favorites.json
  def index
    @favorites = current_user.favorites
  end

  # GET /favorites/new
  def new
    @favorite = Favorite.new
    if Restaurant.all.reject{ |e| current_user.favorites.map{ |e| e.restaurant }.include? e }.size == 0
      redirect_to favorites_path, notice: "Masz już wszystie restauracje w ulubionych"
    end
  end

  # POST /favorites
  # POST /favorites.json
  def create
    @favorite = Favorite.new(favorite_params)
    @favorite.user = current_user

    respond_to do |format|
      if @favorite.save
        format.html { redirect_to favorites_path, notice: 'Pomyślnie dodano restaurację do ulubionych' }
        format.json { render :show, status: :created, location: @favorite }
      else
        format.html { render :new }
        format.json { render json: @favorite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /favorites/1
  # DELETE /favorites/1.json
  def destroy
    @favorite = Favorite.find(params[:id])
    unless current_user.favorites.include? @favorite
      redirect_to favorites_path, notice: "Możesz wyświetlać tylko swoje ulubione restauracje"
    end
    @favorite.destroy
    respond_to do |format|
      format.html { redirect_to favorites_url, notice: 'Favorite was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_favorite
      @favorite = Favorite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def favorite_params
      params.require(:favorite).permit(:restaurant_id)
    end
end
