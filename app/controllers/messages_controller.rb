class MessagesController < ApplicationController
  def new
  	@target_user = User.find(params[:target_id])
    if params[:order_id].present?
      @order = Order.find(params[:order_id])
    end
  end

  def send_mail
  	if params[:order_id].present?
  		@order = Order.find(params[:order_id])
  	else
  		@order = nil
  	end
  	@target = User.find(params[:user_id])
  	@message = params[:message]
  	UserNotifier.private_message(@target, @order, @message, current_user).deliver_now
    redirect_to root_path, notice: "Twoja wiadomość została wysłana"
  end
end
