class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user, index: true, foreign_key: true
      t.references :restaurant, index: true, foreign_key: true
      t.datetime :ordertime
      t.text :desctiption
      t.integer :status

      t.timestamps null: false
    end
  end
end
