class AddPaidToOrderPosition < ActiveRecord::Migration
  def change
    add_column :order_positions, :paid, :float
  end
end
